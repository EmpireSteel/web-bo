import datetime
import time


def get_bo_data(bo, zabbix):
    find = []
    time_from = time.time() - (1 * 6 * 60 * 60)
    find.extend(zabbix.do_request('item.get',{
        'hostids': [bo[0]['hostid']],
        'search':{'name': 'Свободно дискового пространства в / (в процентах)'},
        'output': ['name']
    })['result'])
    find.extend(zabbix.do_request('item.get',{
        'hostids': [bo[0]['hostid']],
        'search':{'name': 'Использование CPU'},
        'output': ['name']
    })['result'])
    find.extend(zabbix.do_request('item.get',{
        'hostids': [bo[0]['hostid']],
        'search':{'name': 'Количество свободной памяти файла подкачки (в процентах)'},
        'output': ['name']
    })['result'])
    find.extend(zabbix.do_request('item.get',{
        'hostids': [bo[0]['hostid']],
        'search':{'name': 'Свободно памяти (в процентах)'},
        'output': ['name']
    })['result'])

    for i in range(len(find)):
        find[i]['data'] = zabbix.do_request('history.get', {
            "history": 0,
            "itemids": find[i]['itemid'],
            'time_from': int(time_from),
            "sortfield": "clock"
        })['result']

    for i in range(len(find)):
        find[i]['time'] = [str(datetime.datetime.fromtimestamp(int(j['clock'])).strftime('%m-%d %H:%M')) for j in find[i]['data']]
        find[i]['values'] = [int(float(j['value'])) for j in find[i]['data']]
        find[i]['data'].clear()
    return find


def get_pos_data(pos, zabbix):
    pos['history'] = {}
    pos['history']['Использование CPU'] = [[] for i in pos['hostid']]
    pos['history']['Свободно памяти (в процентах)'] = [[] for i in pos['hostid']]
    pos['history']['Количество свободной памяти файла подкачки (в процентах)'] = [[] for i in pos['hostid']]
    pos['history']['Свободно дискового пространства в / (в процентах)'] = [[] for i in pos['hostid']]
    cpu_load = []
    time_from = time.time() - (1 * 6 * 60 * 60)
    cpu_load.extend(zabbix.do_request('item.get',{
        'hostids': pos['hostid'],
        'search':{'name': 'Использование CPU'},
        'output': ['itemid']
    })['result'])
    for i in range(len(cpu_load)):
        find = zabbix.do_request('history.get', {
            "history": 0,
            "itemids": cpu_load[i]['itemid'],
            'time_from': int(time_from),
            "sortfield": "clock"
        })['result']
        for j in find:
            pos['history']['Использование CPU'][i].append({'x': j['clock'], 'y': j['value']})

    disk_free = []
    time_from = time.time() - (1 * 6 * 60 * 60)
    disk_free.extend(zabbix.do_request('item.get',{
        'hostids': pos['hostid'],
        'search':{'name': 'Свободно дискового пространства в / (в процентах)'},
        'output': ['itemid']
    })['result'])
    for i in range(len(disk_free)):
        find = zabbix.do_request('history.get', {
            "history": 0,
            "itemids": disk_free[i]['itemid'],
            'time_from': int(time_from),
            "sortfield": "clock"
        })['result']
        for j in find:
            pos['history']['Свободно дискового пространства в / (в процентах)'][i].append({'x': j['clock'], 'y': j['value']})


    pod_free = []
    time_from = time.time() - (1 * 6 * 60 * 60)
    pod_free.extend(zabbix.do_request('item.get',{
        'hostids': pos['hostid'],
        'search':{'name': 'Количество свободной памяти файла подкачки (в процентах)'},
        'output': ['itemid']
    })['result'])
    for i in range(len(pod_free)):
        find = zabbix.do_request('history.get', {
            "history": 0,
            "itemids": pod_free[i]['itemid'],
            'time_from': int(time_from),
            "sortfield": "clock"
        })['result']
        for j in find:
            pos['history']['Количество свободной памяти файла подкачки (в процентах)'][i].append({'x': j['clock'], 'y': j['value']})

    ram_free = []
    time_from = time.time() - (1 * 6 * 60 * 60)
    ram_free.extend(zabbix.do_request('item.get',{
        'hostids': pos['hostid'],
        'search':{'name': 'Свободно памяти (в процентах)'},
        'output': ['itemid']
    })['result'])
    for i in range(len(ram_free)):
        find = zabbix.do_request('history.get', {
            "history": 0,
            "itemids": ram_free[i]['itemid'],
            'time_from': int(time_from),
            "sortfield": "clock"
        })['result']
        for j in find:
            pos['history']['Свободно памяти (в процентах)'][i].append({'x': j['clock'], 'y': j['value']})
    return pos['history']








