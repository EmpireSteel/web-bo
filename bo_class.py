# coding=utf-8
from pyzabbix import ZabbixAPI
import os
import datetime
import psycopg2
import chart_plot


class BO:
    def __init__(self):
        self.zabbix = ZabbixAPI('http://msk-dpro-app351/')
        self.zabbix.login("store_card_api", "12345")
        self.host = ""
        self.hostgroups = ''
        self.bo_applications = {}
        self.pos_applications = {}
        self.bo = []
        self.pos = {}
        self.problems = []
        self.cash = []
        self.tsd = []
        self.bio = []
        self.close_day = []
        self.null_prices = []
        self.last_alk = []
        self.last_check = []
        self.z = []
        self.promo = []
        self.card_graph = []
        self.colors = ['#FF0000', '#FFFF00', '#008000', '#800000#0000FF', '#808080',
                       '#808000', '#00FF00', '#C0C0C0', '#008080', '#0000FF',
                       '#000080', '#FF00FF', '#800080', '#33E3FF', '#FF33A5',
                       '#000000']
        self.version = '2.1.3'

    def get_hostgroups(self):
            self.hostgroups = self.zabbix.do_request('hostgroup.get', {
                'search': {
                    'name': 'RU/Филиал'

                },
                'output': ['groupid', 'name'],
                'sortfield': 'name'
            })['result']
            for i in self.hostgroups:
                i['hosts'] = []
                i['hosts'].extend(self.zabbix.do_request('host.get', {
                    'groupids': i['groupid'],
                    'search': {'host': 'BO*'},
                    'searchWildcardsEnabled': True,
                    'output': ['host'],
                    'sortfield': 'host'
                })['result'])

    def get_bo_applications(self):
        self.bo_applications = {}
        applications = self.zabbix.do_request('application.get', {
            "hostids": self.bo[0]['hostid'],
            'output': ['name']
        })['result']
        for i in applications:
            self.bo_applications[i['name']] = i['applicationid']

    def get_pos_applications(self):
        self.pos['apps'].clear()
        for i in self.pos['hostid']:
            apps = {}
            applications = self.zabbix.do_request('application.get', {
                "hostids": i,
                'output': ['name']
            })['result']
            for i in applications:
                apps[i['name']] = i['applicationid']
            self.pos['apps'].append(apps)

    def get_init(self, page, server):
        self.host = server
        self.get_items()
        self.get_bo_applications()
        self.get_pos_applications()
        if page == 'dashboard':
            self.get_pos_ping_status()
            self.get_uptime()
            self.get_problems()
            print(self.bio)
            self.card_graph = chart_plot.get_bo_data(self.bo, self.zabbix)
        elif page == 'server':
            self.get_bo_processor()
            self.get_bo_utm_monitor()
            self.get_bo_system()
            self.get_bo_memory()
            self.get_bo_os()
            self.get_bo_product()
            self.get_bo_ram()
            self.get_close_day()
        elif page == 'poses':
            self.get_pos_processor()
            self.get_pos_fz54()
            self.get_pos_os()
            self.get_pos_ram()
            self.get_pos_memory()
            self.get_pos_system()
            self.get_pos_product()
            self.pos['history'] = chart_plot.get_pos_data(self.pos, self.zabbix)
            self.get_last_check()
            self.get_last_alk()
            self.get_z()
        elif page == 'network':
            self.get_bo_network()
        elif page == 'promo':
            self.get_promo()
            self.get_null_prices()

    def get_items(self):
        find = self.zabbix.do_request('host.get', {
            'search': {
                'name': self.host
            },
            'selectInterfaces': ['ip'],
            'output': 'extend'
        })['result']
        elements = [{'name': i['name'], 'hostid': i['hostid'], 'ip': i['interfaces'][0]['ip']} for i in find]
        elements = sorted(elements, key=lambda k: k['ip'])
        self.bo = [i for i in elements if i['name'].startswith("BO")]
        self.pos = {'name': [], 'hostid': [], 'ip': [], 'apps': [], 'metrics': {}}
        for i in elements:
            if i['name'].startswith("POS"):
                self.pos['name'].append(i['name'])
                self.pos['hostid'].append(i['hostid'])
                self.pos['ip'].append(i['ip'])
        self.cash = [i for i in elements if i['name'].startswith("CC")]
        self.bio = [i for i in elements if "SAP-" + self.bo[0]['name'][3:] in i['name']]
        tsd = self.zabbix.do_request('item.get', {'hostids': self.bo[0]['hostid'],'search': {'key_': 'tsd.ip'},'output': ['name', 'lastvalue']})['result']
        self.tsd = [{'name': i['name'][7:], 'ip': i['lastvalue']} for i in tsd if str(i['lastvalue']) != '0']
        self.tsd = sorted(self.tsd, key=lambda k: k['ip'])
        self.bo = self.get_ping_status(self.bo)
        self.cash = self.get_ping_status(self.cash)
        self.bio = self.get_ping_status(self.bio)
        self.tsd = self.get_ping_status(self.tsd)

    def get_ping_status(self, item):
        for i in item:
            i['status'] = 'online' if os.system("fping -t 1 " + i['ip']) == 0 else 'offline'
        return item

    def get_pos_ping_status(self):
        self.pos['status'] = []
        for i in self.pos['ip']:
            self.pos['status'].append('online' if os.system("fping -t 1 " + i) == 0 else 'offline')

    def get_uptime(self):
        self.bo[0]['uptime'] = self.zabbix.do_request('item.get', {
            'hostids': self.bo[0]['hostid'],
            'search': {
                'key_': 'system.uptime'
            },
            'output': ['name', 'lastvalue']})["result"][0]['lastvalue']
        self.bo[0]['uptime'] = datetime.timedelta(seconds=int(self.bo[0]['uptime']))
        find = []
        for i in self.pos['hostid']:
            find.extend(self.zabbix.do_request('item.get', {
                'hostids': i,
                'search': {
                    'key_': 'system.uptime'
                },
                'output': ['name', 'lastvalue']})["result"])
        self.pos['uptime'] = [datetime.timedelta(seconds=int(i['lastvalue'])) for i in find]

    def get_bo_processor(self):
        self.bo[0]['processor'] = self.zabbix.do_request("application.get", {
            "applicationids": self.bo_applications['Processor Information'],
            'selectItems': ['name', 'lastvalue']
        })['result'][0]['items']
        for i in range(len(self.bo[0]['processor'])):
            if self.bo[0]['processor'][i]['name'] == 'Использование CPU':
                self.bo[0]['processor'][i]['lastvalue'] = str(round(float(self.bo[0]['processor'][i]['lastvalue']), 2)) + " %"

    def get_bo_utm_monitor(self):
        self.bo[0]['utm_monitor'] = self.zabbix.do_request("application.get", {
            "applicationids": self.bo_applications['UTM-MONITOR'],
            'selectItems': ['name', 'lastvalue']
        })['result'][0]['items']

    def get_bo_system(self):
        self.bo[0]['system'] = self.zabbix.do_request("application.get", {
            "applicationids": self.bo_applications['System'],
            'selectItems': ['name', 'lastvalue']
        })['result'][0]['items']
        for i in range(len(self.bo[0]['system'])):
            if self.bo[0]['system'][i]['name'] == 'Аптайм':
                self.bo[0]['system'][i]['lastvalue'] = datetime.timedelta(seconds=int(self.bo[0]['system'][i]['lastvalue']))
            if self.bo[0]['system'][i]['name'] == 'Местное время':
                self.bo[0]['system'][i]['lastvalue'] = datetime.datetime.fromtimestamp(int(self.bo[0]['system'][i]['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S')

    def get_bo_memory(self):
        self.bo[0]['memory'] = self.zabbix.do_request("application.get", {
            "applicationids": self.bo_applications['Memory'],
            'selectItems': ['name', 'lastvalue']
        })['result'][0]['items']
        for i in self.bo[0]['memory']:
            if i['name'] == "Общее количество дискового пространства в /" or \
                    i['name'] == "Свободно дискового пространства в / (GB)":
                i['lastvalue'] = str(round(float(i['lastvalue']) / 1024 / 1024 / 1024, 2)) + " Gb"
            if i['name'] == 'Свободно дискового пространства в / (в процентах)':
                i['lastvalue'] = str(round(float(i['lastvalue']), 2)) + " %"

    def get_bo_os(self):
        self.bo[0]['os'] = self.zabbix.do_request("application.get", {
            "applicationids": self.bo_applications['OS Information'],
            'selectItems': ['name', 'lastvalue']
        })['result'][0]['items']

    def get_bo_product(self):
        self.bo[0]['product'] = self.zabbix.do_request("application.get", {
            "applicationids": self.bo_applications['Product Information'],
            'selectItems': ['name', 'lastvalue']
        })['result'][0]['items']

    def get_bo_ram(self):
        self.bo[0]['ram'] = self.zabbix.do_request("application.get", {
            "applicationids": self.bo_applications['RAM'],
            'selectItems': ['name', 'lastvalue']
        })['result'][0]['items']
        for i in self.bo[0]['ram']:
            if i['name'] == 'Количество используемой памяти' \
                    or i['name'] == 'Общее количество памяти' or i['name'] == 'Свободно памяти':
                i['lastvalue'] = str(round(float(i['lastvalue']) / 1024 / 1024 / 1024, 2)) + " Gb"
            if i['name'] == 'Количество свободной памяти файла подкачки (в процентах)':
                i['lastvalue'] = str(round(float(i['lastvalue']), 2)) + " %"

    def get_bo_network(self):
        self.bo[0]['network'] = self.zabbix.do_request("application.get", {
            "applicationids": self.bo_applications['Router'],
            'selectItems': ['name', 'lastvalue']
        })['result'][0]['items']

    def get_close_day(self):
        conn_string = "host=" + str(self.bo[0]['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
                        SELECT ra."create_timestamp"
        FROM gk_report_archive ra
        WHERE ra."type_code" = 'XRG_TILL_FINANCIAL'
        ORDER BY ra."create_timestamp" DESC
        LIMIT 1
                """)
        records = cursor.fetchall()
        conn.close()
        self.bo[0]['close_day'] = [{'date': i[0]} for i in records]

    def get_pos_processor(self):
        items = {}
        for i in self.pos['apps']:
            result = self.zabbix.do_request("application.get", {
                "applicationids": i['Processor Information'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            result = sorted(result, key=lambda k: k['name'])
            for j in result:
                if j['name'] not in items.keys():
                    items[j['name']] = []

                if j['name'] == 'Использование CPU':
                    items[j['name']].append(str(round(float(j['lastvalue']), 2)) + " %")
                else:
                    if len(j['lastvalue']) >= 18:
                        items[j['name']].append(j['lastvalue'][:18] + "\n" + j['lastvalue'][18:])
                    else:
                        items[j['name']].append(j['lastvalue'])
        self.pos['metrics']['Процессор'] = items

    def get_pos_fz54(self):
        items = {}
        for i in self.pos['apps']:
            result = self.zabbix.do_request("application.get", {
                "applicationids": i['FZ54'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            result = sorted(result, key=lambda k: k['name'])
            for j in result:
                if j['name'] not in items.keys():
                    items[j['name']] = []
                if j['name'] == 'Дата активизации ФН':
                    items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                elif j['name'] == 'Дата последнего документа отправленного в ОФД':
                    if int(j['lastvalue']) != 0:
                        items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                    else:
                        items[j['name']].append('никогда')
                elif j['name'] == 'Дата создания файла info_FR_human':
                    if int(j['lastvalue']) != 0:
                        items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                    else:
                        items[j['name']].append('никогда')
                else:
                    items[j['name']].append(j['lastvalue'])
        self.pos['metrics']['ФЗ54'] = items

    def get_pos_init(self):
        items = {}
        for i in self.pos['apps']:
            result = self.zabbix.do_request("application.get", {
                "applicationids": i['Initialization'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            result = sorted(result, key=lambda k: k['name'])
            for j in result:
                if j['name'] not in items.keys():
                    items[j['name']] = []
                items[j['name']].append(j['lastvalue'])
        self.pos['metrics']['Инициализация устройств'] = items

    def get_pos_os(self):
        items = {}
        for i in self.pos['apps']:
            result = self.zabbix.do_request("application.get", {
                "applicationids": i['OS Information'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            result = sorted(result, key=lambda k: k['name'])
            for j in result:
                if j['name'] not in items.keys():
                    items[j['name']] = []
                items[j['name']].append(j['lastvalue'])
        self.pos['metrics']['Операционная система'] = items

    def get_pos_ram(self):
        items = {}
        for i in self.pos['apps']:
            result = self.zabbix.do_request("application.get", {
                "applicationids": i['RAM'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            result = sorted(result, key=lambda k: k['name'])
            for j in result:
                if j['name'] not in items.keys():
                    items[j['name']] = []
                if "в процентах" in j['name']:
                    items[j['name']].append(str(round(float(j['lastvalue']), 2)) + " %")
                else:
                    items[j['name']].append(str(round(float(j['lastvalue']) / 1024 / 1024, 2)) + " Mb")
        self.pos['metrics']['Оперативная память'] = items

    def get_pos_memory(self):
        items = {}
        for i in self.pos['apps']:
            result = self.zabbix.do_request("application.get", {
                "applicationids": i['Memory'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            result = sorted(result, key=lambda k: k['name'])
            for j in result:
                if j['name'] not in items.keys():
                    items[j['name']] = []
                if "в процентах" in j['name']:
                    items[j['name']].append(str(round(float(j['lastvalue']), 2)) + " %")
                elif "RAID" in j['name']:
                    items[j['name']].append(j['lastvalue'])
                else:
                    items[j['name']].append(str(round(float(j['lastvalue']) / 1024 / 1024 / 1024, 2)) + " Gb")
        self.pos['metrics']['Память'] = items

    def get_pos_system(self):
        items = {}
        for i in self.pos['apps']:
            result = self.zabbix.do_request("application.get", {
                "applicationids": i['System'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            result = sorted(result, key=lambda k: k['name'])
            for j in result:
                if j['name'] not in items.keys():
                    items[j['name']] = []
                if j['name'] == 'Аптайм':
                    items[j['name']].append(datetime.timedelta(seconds=int(j['lastvalue'])))
                elif j['name'] == 'Местное время':
                    items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                else:
                    items[j['name']].append(j['lastvalue'])
            self.pos['metrics']['Система'] = items

    def get_pos_product(self):
        items = {}
        for i in self.pos['apps']:
            result = self.zabbix.do_request("application.get", {
                "applicationids": i['Product Information'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            result = sorted(result, key=lambda k: k['name'])
            for j in result:
                if j['name'] not in items.keys():
                    items[j['name']] = []
                items[j['name']].append(j['lastvalue'])
            self.pos['metrics']['Устройство'] = items

    def get_problems(self):
        pos_find = []
        self.problems = []
        bo_find = self.zabbix.do_request("trigger.get", {
            "hostids": self.bo[0]['hostid'],
            "expandDescription": "1",
            "output": 'extend',
            "filter": {'value': 1},
        })['result']
        for i in bo_find:
            self.problems.append({'name': i['description'], 'host': self.bo[0]['name'],
                                   'date': datetime.datetime.fromtimestamp(int(i['lastchange']))
                                        .strftime('%Y-%m-%d %H:%M:%S')})
        for i in self.pos['name']:
            pos_find = self.zabbix.do_request("trigger.get", {
                "host": i,
                "expandDescription": "1",
                "output": 'extend',
                "filter": {'value': 1},
            })['result']
            for j in pos_find:
                self.problems.append({'name': j['description'], 'host': i,
                                      'date': datetime.datetime.fromtimestamp(int(j['lastchange']))
                                     .strftime('%Y-%m-%d %H:%M:%S')})
        self.problems = sorted(self.problems, key=lambda k: k['date'], reverse=True)

    def get_last_check(self):
        conn_string = "host=" + str(self.bo[0]['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        SELECT 
        max(bk.aktdat)
        ,bk."workstation_id"
        FROM "gkretail"."gk_bonkopf" bk
        GROUP BY bk."workstation_id"
        ORDER BY bk."workstation_id"
        """)
        records = cursor.fetchall()
        records = records[1:]
        conn.close()
        self.pos['last_check'] = [{'pos': i, 'date': 'нет данных'} for i in self.pos['name']]
        for i in self.pos['last_check']:
            for j in records:
                if int(i['pos'][3:5]) == j[1]:
                    i['date'] = j[0]

    def get_last_alk(self):
        conn_string = "host=" + str(self.bo[0]['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
                SELECT DISTINCT on (y.workstation_id) 
        max(y."aktdat")
        ,y.workstation_id
        ,nn.nm_itm
        FROM
        gkretail.gk_bonkopf y
        ,gkretail.gk_bonposition z
        ,gkretail.as_itm nn
        WHERE
        z.artnr IN (SELECT item_id FROM xrg_item WHERE alcohol_flag = 'J' and TAX_INPUT_REQUIRED_FLAG='J')
        AND z.bon_seq_id = y.bon_seq_id
        AND nn.id_itm = z.artnr
        GROUP BY y.workstation_id, y.aktdat, nn.nm_itm
        ORDER BY y.workstation_id ASC, y.aktdat DESC
        """)
        records = cursor.fetchall()
        conn.close()
        self.pos['last_alk'] = [{'pos': i, 'date': 'нет данных', 'item': 'нет данных'} for i in self.pos['name']]
        for i in self.pos['last_alk']:
            for j in records:
                if int(i['pos'][3:5]) == j[1]:
                    i['date'] = j[0]
                    i['item'] = j[2]

    def get_z(self):
        conn_string = "host=" + str(self.bo[0]['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        SELECT DISTINCT on (b."lade_nr")
        max(b."aktdat")
       ,b."lade_nr"
        FROM "gkretail"."gk_bonkopf" b
        WHERE b."workstation_id" = 0
        AND b."belegtyp" = 501
        GROUP BY b."lade_nr", "aktdat"
        ORDER BY b."lade_nr" ASC, b."aktdat" DESC
                """)
        records = cursor.fetchall()
        conn.close()

        self.pos['z'] = [{'pos': i, 'date': 'нет данных'} for i in self.pos['name']]
        for i in self.pos['z']:
            for j in records:
                if int(i['pos'][3:5]) == j[1]:
                    i['date'] = j[0]

    def get_null_prices(self):
        conn_string = "host=" + str(self.bo[0]['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        select
        ids.item_id, names.de_itm, prices.price_amount
        from xrg_item as ids 
        join as_itm as names on ids.item_id=names.id_itm
        join gk_item_selling_prices as prices on ids.item_id=prices.item_id
        where 
        prices.price_amount = 0
        order by ids.item_id
        """)
        records = cursor.fetchall()
        conn.close()
        self.null_prices = []
        for i in records:
            self.null_prices.append({'ids': i[0], 'item': i[1]})

    def get_promo(self):
        conn_string = "host=" + str(self.bo[0]['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
 SELECT
"nm_prm_prt"
, "description"
FROM "gkretail"."co_prm"
ORDER BY "nm_prm_prt" DESC
                   """)
        records = cursor.fetchall()
        conn.close()
        self.promo = []
        for i in records:
            self.promo.append({'name': i[0], 'description': i[1]})






