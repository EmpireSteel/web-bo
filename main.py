# coding=utf-8

from flask import Flask, render_template, redirect, request, url_for
from bo_class import BO


app = Flask(__name__)
bo = BO()


@app.route('/')
def main():
    find = request.args.get('store')
    if not find:
        bo.get_hostgroups()
        return render_template('start.html', bo=bo)
    else:
        return redirect(url_for('pages', page='dashboard', server=find))


@app.route('/<page>/<server>')
def pages(page, server):
    find = request.args.get('store')
    if not find:
        bo.get_hostgroups()
        if server.upper().startswith("BO-"):
            server = server[3:]
        bo.get_init(page, server)
        return render_template(page + '.html', bo=bo)
    else:
        return redirect(url_for('pages', page=page, server=find))


if __name__ == '__main__':
    app.run(debug=True)
