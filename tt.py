from pyzabbix import ZabbixAPI
import time
import datetime

zabbix = ZabbixAPI('http://msk-dpro-app351/')
zabbix.login("store_card_api", "12345")
hostid = ['90599', '55675', '55680', '55683']

def test(pos):
    pos['history'] = {}
    pos['history']['disk_free'] = [[] for i in pos['hostid']]
    pos['history']['pod_free'] = [[] for i in pos['hostid']]
    pos['history']['cpu_load'] = [[] for i in pos['hostid']]
    pos['history']['ram_free'] = [[] for i in pos['hostid']]

    cpu_load = []
    time_from = time.time() - (1 * 6 * 60 * 60)
    cpu_load.extend(zabbix.do_request('item.get',{
        'hostids': pos['hostid'],
        'search':{'name': 'Использование CPU'},
        'output': ['itemid']
    })['result'])
    for i in range(len(cpu_load)):
        find = zabbix.do_request('history.get', {
            "history": 0,
            "itemids": cpu_load[i]['itemid'],
            'time_from': int(time_from),
            "sortfield": "clock"
        })['result']
        for j in find:
            pos['history']['cpu_load'][i].append({'x': j['clock'], 'y': j['value']})

    disk_free = []
    time_from = time.time() - (1 * 6 * 60 * 60)
    disk_free.extend(zabbix.do_request('item.get',{
        'hostids': pos['hostid'],
        'search':{'name': 'Свободно дискового пространства в / (в процентах)'},
        'output': ['itemid']
    })['result'])
    for i in range(len(disk_free)):
        find = zabbix.do_request('history.get', {
            "history": 0,
            "itemids": disk_free[i]['itemid'],
            'time_from': int(time_from),
            "sortfield": "clock"
        })['result']
        for j in find:
            pos['history']['disk_free'][i].append({'x': j['clock'], 'y': j['value']})


    pod_free = []
    time_from = time.time() - (1 * 6 * 60 * 60)
    pod_free.extend(zabbix.do_request('item.get',{
        'hostids': pos['hostid'],
        'search':{'name': 'Количество свободной памяти файла подкачки (в процентах)'},
        'output': ['itemid']
    })['result'])
    for i in range(len(pod_free)):
        find = zabbix.do_request('history.get', {
            "history": 0,
            "itemids": pod_free[i]['itemid'],
            'time_from': int(time_from),
            "sortfield": "clock"
        })['result']
        for j in find:
            pos['history']['pod_free'][i].append({'x': j['clock'], 'y': j['value']})

    ram_free = []
    time_from = time.time() - (1 * 6 * 60 * 60)
    ram_free.extend(zabbix.do_request('item.get',{
        'hostids': pos['hostid'],
        'search':{'name': 'Свободно памяти (в процентах)'},
        'output': ['itemid']
    })['result'])
    for i in range(len(ram_free)):
        find = zabbix.do_request('history.get', {
            "history": 0,
            "itemids": ram_free[i]['itemid'],
            'time_from': int(time_from),
            "sortfield": "clock"
        })['result']
        for j in find:
            pos['history']['ram_free'][i].append({'x': j['clock'], 'y': j['value']})
    return pos['history']